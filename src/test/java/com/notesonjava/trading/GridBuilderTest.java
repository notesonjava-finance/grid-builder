package com.notesonjava.trading;

import java.math.BigDecimal;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.notesonjava.trading.model.RequestOrder;

public class GridBuilderTest {
	
	private GridBuilder builder;
	
	@BeforeEach
	public void setup() {
		builder = new GridBuilder();
	}
	
	@Test
	public void createUpperGrid_btc() {
		BigDecimal amount = new BigDecimal(0.5);
		BigDecimal lowPrice = new BigDecimal(20000);
		BigDecimal highPrice = new BigDecimal(25000);
		int gridSize = 20;

		List<RequestOrder> grid = builder.createUpperGrid(amount, lowPrice, highPrice, gridSize);
		grid.forEach(order -> {
			System.out.println(order);
			System.out.println(order.getTotalPrice());
		});
		
		assertThat(grid).hasSize(20);
		assertThat(grid.get(0).getPrice()).isEqualTo(BigDecimal.valueOf(20000));
		assertThat(grid.get(19).getPrice()).isEqualTo(BigDecimal.valueOf(25000));
		
		
		double sum = grid.stream().mapToDouble(o -> o.getQuantity().doubleValue()).sum();
		assertThat(sum).isEqualTo(amount.doubleValue(), Offset.offset(0.1));
		assertThat(sum).isLessThanOrEqualTo(amount.doubleValue());

	}
	
	@Test
	public void createBottomGrid_btc() {
		BigDecimal amount = new BigDecimal(0.5);
		BigDecimal endPrice = new BigDecimal(16000);
		BigDecimal startPrice = new BigDecimal(20000);
		int gridSize = 20;

		List<RequestOrder> grid = builder.createBottomGrid(amount, startPrice, endPrice, gridSize);
		grid.forEach(order -> {
			System.out.println(order);
			System.out.println(order.getTotalPrice());
		});
		
		assertThat(grid).hasSize(20);
		assertThat(grid.get(0).getPrice()).isEqualTo(BigDecimal.valueOf(20000));
		assertThat(grid.get(19).getPrice()).isEqualTo(BigDecimal.valueOf(16000));
		
		
		double sum = grid.stream().mapToDouble(o -> o.getQuantity().doubleValue()).sum();
		assertThat(sum).isEqualTo(amount.doubleValue(), Offset.offset(0.1));
		assertThat(sum).isLessThanOrEqualTo(amount.doubleValue());

	}
	
}

