package com.notesonjava.trading;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.assertj.core.api.Assertions;
import org.assertj.core.data.Offset;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AmountCalculatorTest {
	
	private AmountCalculator calculator;
	private static final Offset<Double> OFFSET = Offset.offset(0.000001);
	
	@BeforeEach
	public void setup() {
		calculator = new AmountCalculator();
	}
	
	@Test
	public void calculateAmountGrid() {
		BigDecimal amount = new BigDecimal("0.5");
		int gridSize = 100;
		List<BigDecimal> result = calculator.calculateAmounts(amount, gridSize);
		result.forEach(System.out::println);
		Assertions.assertThat(result).hasSize(gridSize);
		BigDecimal sum = BigDecimal.ZERO;
		for(BigDecimal level : result) {
			sum = sum.add(level);
		}
		Assertions.assertThat(sum.doubleValue()).isEqualTo(amount.doubleValue(), OFFSET);
		List<BigDecimal> roundedList = result.stream().map(level -> level.setScale(4, RoundingMode.HALF_UP)).toList();
		
		sum = BigDecimal.ZERO;
		for(BigDecimal level : roundedList) {
			sum = sum.add(level);
		}
		Assertions.assertThat(sum.doubleValue()).isEqualTo(amount.doubleValue(), OFFSET);
		BigDecimal previous = null;
		for(BigDecimal value: result) {
			if(previous != null) {	
				Assertions.assertThat(value.compareTo(previous)).isEqualTo(1);
			}
			previous = value;
		}
	}

	@Test
	public void calculateAmountGridWithInitialAmount() {
		BigDecimal amount = new BigDecimal("0.5");
		int gridSize = 20;
		List<BigDecimal> result = calculator.calculateAmounts(amount, new BigDecimal("0.01") ,gridSize);
		result.forEach(System.out::println);
		
		Assertions.assertThat(result).hasSize(gridSize);
		BigDecimal sum = BigDecimal.ZERO;
		for(BigDecimal level : result) {
			sum = sum.add(level);
		}
		Assertions.assertThat(sum.doubleValue()).isEqualTo(amount.doubleValue(), OFFSET);
		
		BigDecimal previous = null;
		for(BigDecimal value: result) {
			if(previous != null) {	
				Assertions.assertThat(value.compareTo(previous)).isEqualTo(1);
			}
			previous = value;
		}
		
	}

	
}
