package com.notesonjava.trading;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AmountCalculator {
	
	public static List<BigDecimal> calculateAmounts(BigDecimal amount, int gridSize) {
		
		int precision = 8;
		int partSum = IntStream.rangeClosed(1, gridSize).sum();
		BigDecimal partSize = amount.divide(BigDecimal.valueOf(partSum), precision, RoundingMode.HALF_UP);
		
		List<BigDecimal> result = new ArrayList<>();
		for(int i = 1; i <= gridSize; i++) {
			BigDecimal index = BigDecimal.valueOf(i);
			BigDecimal value = index.multiply(partSize);
			result.add(value);
		}
		return Collections.unmodifiableList(result);
	}

	public static List<BigDecimal> calculateAmounts(BigDecimal amount, BigDecimal minAmount, int gridSize) {
		BigDecimal minimumValid = minAmount.multiply(BigDecimal.valueOf(gridSize));
		if(minimumValid.compareTo(amount) > 0) {
			log.error("Invalid amount, amount must be bigger than minAmount * gridSize");
			return new ArrayList<>();
		}
		BigDecimal amountToSplit = amount.subtract(minAmount.multiply(BigDecimal.valueOf(gridSize)));
		List<BigDecimal> levels = calculateAmounts(amountToSplit, gridSize - 1);
		List<BigDecimal> result = new ArrayList<>();
		result.add(minAmount);
		for(int i = 0; i < levels.size(); i++) {
			BigDecimal value = levels.get(i).add(minAmount); 
			result.add(value);
		}
		return Collections.unmodifiableList(result);

	}

	
}
