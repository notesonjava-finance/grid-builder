package com.notesonjava.trading;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import com.notesonjava.trading.model.RequestOrder;

public class GridBuilder {
	
	
	
	public List<RequestOrder> createUpperGrid(BigDecimal amount, BigDecimal startPrice, BigDecimal endPrice, int gridSize) {
		
		
		List<BigDecimal> levels = AmountCalculator.calculateAmounts(amount, gridSize);
		
		
		BigDecimal priceIncrement = endPrice.subtract(startPrice).divide(BigDecimal.valueOf(gridSize - 1), RoundingMode.HALF_UP);
		List<RequestOrder> result = new ArrayList<>();
		for(int i = 0; i < levels.size(); i++) {
			RequestOrder order = new RequestOrder();
			order.setQuantity(levels.get(i));
			BigDecimal increment = priceIncrement.multiply(BigDecimal.valueOf(i));
			order.setPrice(startPrice.add(increment));
			result.add(order);
		}
		result.get(result.size() - 1).setPrice(endPrice);
		return result;
	}
	
	public List<RequestOrder> createBottomGrid(BigDecimal amount, BigDecimal startPrice, BigDecimal endPrice, int gridSize) {
		
		
		List<BigDecimal> levels = AmountCalculator.calculateAmounts(amount, gridSize);
		
		BigDecimal priceDecrement = startPrice.subtract(endPrice).divide(BigDecimal.valueOf(gridSize -1),  RoundingMode.HALF_UP);
		List<RequestOrder> result = new ArrayList<>();
		for(int i = 0; i < levels.size(); i++) {
			RequestOrder order = new RequestOrder();
			order.setQuantity(levels.get(i));
			BigDecimal decrement = priceDecrement.multiply(BigDecimal.valueOf(i));
			order.setPrice(startPrice.subtract(decrement));
			result.add(order);
		}
		result.get(result.size() - 1).setPrice(endPrice);
		return result;
	}
	
		
}
