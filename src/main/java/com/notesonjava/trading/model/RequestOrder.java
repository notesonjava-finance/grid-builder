package com.notesonjava.trading.model;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RequestOrder {

	private BigDecimal quantity;
	private BigDecimal price;
	
	public BigDecimal getTotalPrice() {
		return quantity.multiply(price);
	}
	
}
